package heritage;

public class Lion extends Felin {
    //Constructeur par défaut :
    public Lion() {

    }

    //Constructeur avec paramètres
    public Lion(String couleur, int poids) {
        this.couleur = couleur;
        this.poids = poids;
    }

    //Méthode :
    void crier() {
        System.out.println("Je rugis dans la savane !");
    }
}
