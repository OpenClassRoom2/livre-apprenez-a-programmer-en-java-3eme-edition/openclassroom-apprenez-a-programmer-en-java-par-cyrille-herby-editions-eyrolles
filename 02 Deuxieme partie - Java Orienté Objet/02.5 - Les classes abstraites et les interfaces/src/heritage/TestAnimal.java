package heritage;

public class TestAnimal {
    public static void main(String[] args) {
        Loup loup = new Loup("Gris bleuté", 20);
        loup.boire();
        loup.manger();
        loup.deplacement();
        loup.crier();
        System.out.println(loup.toString());

        //Les méthodes d'un chien :
        System.out.println("LES METHODES D'UN CHIEN");
        Chien chien = new Chien("Gris bleuté", 20);
        chien.boire();
        chien.manger();
        chien.deplacement();
        chien.crier(); System.out.println(chien.toString());
        System.out.println("-------------------------------------------- ");

        //les méthodes de l'interface :
        System.out.println("LES METHODES DE L'INTERFACE");
        chien.faireCalin();
        chien.faireLeBeau();
        chien.faireLechouille();
        System.out.println("-------------------------------------------- ");

        //Utilisons le polymorphisme de notre interface.
        System.out.println("POLYMORPHISME DE L'INTERFACE");
        Rintintin rintintin = new Chien();
        rintintin.faireLeBeau();
        rintintin.faireCalin();
        rintintin.faireLechouille();
    }
}
