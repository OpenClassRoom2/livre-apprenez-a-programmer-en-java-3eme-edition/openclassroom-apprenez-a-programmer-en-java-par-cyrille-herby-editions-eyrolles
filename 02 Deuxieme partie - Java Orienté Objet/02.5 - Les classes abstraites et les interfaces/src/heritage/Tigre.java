package heritage;

public class Tigre extends Felin {
    //Constructeur par défaut :
    public Tigre() {

    }

    //Constructeur avec paramètres
    public Tigre(String couleur, int poids) {
        this.couleur = couleur;
        this.poids = poids;
    }

    //Méthode :
    void crier() {
        System.out.println("Je grogne très fort !");
    }
}
