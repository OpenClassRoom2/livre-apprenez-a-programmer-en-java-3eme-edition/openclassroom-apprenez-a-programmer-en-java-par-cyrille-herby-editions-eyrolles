package heritage;

public class Chat extends Felin {
    //Constructeur par défaut :
    public Chat() {

    }

    //Constructeur avec paramètres
    public Chat(String couleur, int poids) {
        this.couleur = couleur;
        this.poids = poids;
    }

    //Méthode :
    void crier() {
        System.out.println("Je miaule sur les toits !");
    }
}
