package heritage;

public class Loup extends Canin{
    //Constructeur par défaut :
    public Loup() {

    }

    //Constructeur avec paramètres
    public Loup(String couleur, int poids) {
        this.couleur = couleur;
        this.poids = poids;
    }

    //Méthode :
    void crier() {
        System.out.println("Je hurle à la Lune en faisant ouhouh !");
    }
}
