package heritage;

/* La classe Chien hérite de la classe abstraite Canin et implemente les méthodes de l'interface Rintintin. */
public class Chien extends Canin implements Rintintin{
    //Constructeur par défaut :
    public Chien() {

    }

    //Constructeur avec paramètres
    public Chien(String couleur, int poids) {
        this.couleur = couleur;
        this.poids = poids;
    }

    //Méthodes :
    void crier() { System.out.println("J'aboie sans raison !"); }

    @Override
    public void faireCalin() { System.out.println("Je te fais un GROS CÂlin"); }

    @Override
    public void faireLechouille() { System.out.println("Je fais le beau !"); }

    @Override
    public void faireLeBeau() { System.out.println("Je fais de grosses léchouilles..."); }
}
