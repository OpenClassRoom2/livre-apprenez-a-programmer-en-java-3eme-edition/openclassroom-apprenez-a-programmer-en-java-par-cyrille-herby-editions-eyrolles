package encapsulation;

import encapsulation.comportement.Deplacement;
import encapsulation.comportement.EspritCombatif;
import encapsulation.comportement.Operation;
import encapsulation.comportement.Soin;

/**
 * Classe qui permet de créer un Chirugien/
 * @author Enrick Enet
 */
public class Chirurgien extends Personnage {

    /**
     * Constructeur par défaut.
     */
    public Chirurgien() {
        this.soin = new Operation();
    }

    /**
     * Constructeur avec paramètres.
     * @param espritCombatif - Un espritCombatif de type EspritCombatif.
     * @param soin - Un soin de type Soin.
     * @param deplacement - Un deplacement de type Deplacement.
     */
    public Chirurgien(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
        super(espritCombatif, soin, deplacement);
    }
}
