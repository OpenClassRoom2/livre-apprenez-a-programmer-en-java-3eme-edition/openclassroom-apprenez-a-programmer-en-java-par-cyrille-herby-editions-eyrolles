package encapsulation;

import encapsulation.comportement.Deplacement;
import encapsulation.comportement.EspritCombatif;
import encapsulation.comportement.PremierSoin;
import encapsulation.comportement.Soin;

/**
 * Classe qui permet de créer un Médecin.
 * @author Enrick Enet
 */
public class Medecin extends Personnage{

    /**
     * Constructeur par défaut.
     */
    public Medecin() {
        this.soin = new PremierSoin();
    }

    /**
     * Constructeur avec paramètres.
     * @param espritCombatif - Un espritCombatif de type EspritCombatif.
     * @param soin - Un soin de type Soin.
     * @param deplacement - Un deplacement de type Deplacement.
     */
    public Medecin(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
        super(espritCombatif, soin, deplacement);
    }
}
