package encapsulation;

import encapsulation.comportement.Deplacement;
import encapsulation.comportement.EspritCombatif;
import encapsulation.comportement.Pacifiste;
import encapsulation.comportement.Soin;

/**
 * Classe qui permet de créer un Civil.
 * @author Enrick Enet
 */
public class Civil extends Personnage{

    /**
     * Constructeur par défaut.
     */
    public Civil() {
        this.espritCombatif = new Pacifiste();
    }

    /**
     * Constructeur avec paramètres.
     * @param espritCombatif - Un espritCombatif de type EspritCombatif.
     * @param soin - Un soin de type Soin.
     * @param deplacement - Un deplacement de type Deplacement.
     */
    public Civil(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
        super(espritCombatif, soin, deplacement);
    }
}
