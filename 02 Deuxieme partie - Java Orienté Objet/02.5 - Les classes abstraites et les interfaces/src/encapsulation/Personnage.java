package encapsulation;

import encapsulation.comportement.*;

import java.text.DecimalFormat;

public abstract class Personnage {
    protected EspritCombatif espritCombatif = new Pacifiste();
    protected Soin soin = new AucunSoin();
    protected Deplacement deplacement = new Marcher();

    /**
     * Constructeur par défaut.
     */
    public Personnage() {
    }

    /**
     * Constructeur avec paramètres.
     * @param espritCombatif - Un objet de type EspritCombatif.
     * @param soin - Un objet de type Soin.
     * @param deplacement - Un objet de type Deplacement.
     */
    public Personnage(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
        setEspritCombatif(espritCombatif);
        setSoin(soin);
        setDeplacement(deplacement);
    }

    //Méthode de déplacement de personnage.
    public void seDeplacer() {
        //On utilise les objets de déplacement de façon polymorphe
        deplacement.deplacer();
    }

    //Méthode que les combattants utilisent.
    public void combattre() {
        //On utilise les objets de espritCombatif de façon polymorphe
        espritCombatif.combat();
    }

    public void soigner() {
        //On utilise les objets de soin de façon polymorphe
        soin.soigne();
    }

    //Accesseur et mutateurs

    /**
     * Redéfinit le comportement au combat
     * @param espritCombatif - Un objet de type EspritCombatif.
     */
    public void setEspritCombatif(EspritCombatif espritCombatif) {
        this.espritCombatif = espritCombatif;
    }

    /**
     * Redéfinit le comportement de soin.
     * @param soin - un objet de type Soin.
     */
    public void setSoin(Soin soin) {
        this.soin = soin;
    }

    /**
     * Redéfinit le comportement de deplacement.
     * @param deplacement - Un objet de type Deplacement.
     */
    public void setDeplacement(Deplacement deplacement) {
        this.deplacement = deplacement;
    }
}
