package encapsulation;

import encapsulation.comportement.CombatPistolet;
import encapsulation.comportement.Deplacement;
import encapsulation.comportement.EspritCombatif;
import encapsulation.comportement.Soin;

/**
 * Classe qui permet de créer un Guerrier;
 * @author Enrick Enet
 */
public class Guerrier extends Personnage{

    /**
     * Constructeur par défaut.
     */
    public Guerrier() {
        this.espritCombatif = new CombatPistolet();
    }


    /**
     * Constructeur avec paramètres.
     * @param espritCombatif - Un espritCombatif de type EspritCombatif.
     * @param soin - Un soin de type Soin.
     * @param deplacement - Un deplacement de type Deplacement.
     */
    public Guerrier(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
        super(espritCombatif,soin,deplacement);
    }
}
