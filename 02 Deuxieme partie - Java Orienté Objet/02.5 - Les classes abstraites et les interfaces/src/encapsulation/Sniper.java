package encapsulation;

import encapsulation.comportement.Deplacement;
import encapsulation.comportement.EspritCombatif;
import encapsulation.comportement.Soin;

/**
 * Classe qui permet de créer un sniper.
 * @author Enrick Enet
 */
public class Sniper extends Personnage{

    /**
     * Constructeur par défaut.
     */
    public Sniper() {
    }

    /**
     * Constructeur avec paramètres.
     * @param espritCombatif - Un espritCombatif de type EspritCombatif.
     * @param soin - Un soin de type Soin.
     * @param deplacement - Un deplacement de type Deplacement.
     */
    public Sniper(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
        super(espritCombatif, soin, deplacement);
    }
}
