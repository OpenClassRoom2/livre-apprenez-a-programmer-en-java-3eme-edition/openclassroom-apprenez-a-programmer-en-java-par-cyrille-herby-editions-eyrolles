package encapsulation;

import encapsulation.comportement.Operation;

public class TestPersonnage {
    public static void main(String[] args) {

        Personnage[] listePersonnage = {    new Guerrier(),
                                            new Chirurgien(),
                                            new Civil(),
                                            new Medecin(),
                                            new Sniper()
                                        };



        for(int i = 0; i < listePersonnage.length; i++) {
            System.out.println("\nInstance de " + listePersonnage[i].getClass().getName());
            System.out.println("*****************************************");
            listePersonnage[i].combattre();
            listePersonnage[i].seDeplacer();
            listePersonnage[i].soigner(); }

        //On redéfinit le comportement d'un guerrier afin qu'il puisse soigner.
        System.out.println("");
        System.out.println("***** GUERRIER QUI SOIGNE ****");
        Personnage guerrierQuiSoigne = new Guerrier();
        guerrierQuiSoigne.setSoin(new Operation());
        guerrierQuiSoigne.soigner();
    }
}
