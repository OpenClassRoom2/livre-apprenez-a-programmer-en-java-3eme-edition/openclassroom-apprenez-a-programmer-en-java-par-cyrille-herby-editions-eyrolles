public class Capitale extends Ville {
    //Attributs :
    private String monument;

    /**
     * Constructeur par défaut/
     */
    public Capitale(){
        super(); //Appelle le constructeur de la classe mère
        monument = "aucun";
    }

    /**
     * Constructeur d'initialisation de capitale.
     * @param pNom - le nom de la ville
     * @param pNbre - le nombre d'habitants
     * @param pPays - le nom du pays
     * @param monument - le nom du monument
     */
    public Capitale(String pNom, int pNbre, String pPays, String monument) {
        super(pNom, pNbre, pPays);
        setMonument(monument);
    }

    /**
     * Méthode qui permet de décrire un monument.
     * @return - La description de l'objet.
     */
    public String decrisToi() {
        String str = super.decrisToi() + "\n \t ==>>" + this.monument + " en est un monument";
        //System.out.println("Invocation de super.decrisToi()");
        return str;
    }

    //Accesseurs et mutateurs :
    /**
     * @return - le nom du monument.
     */
    public String getMonument() { return monument; }

    /**
     * Définit le nom du monument.
     * @param monument - Le nom du monument.
     */
    public void setMonument(String monument) { this.monument = monument; }

    //Autres Méthodes :

    /**
     * Permet de présenter l'objet Capitale en chaine de caractères.
     * @return - une chaine de caractères.
     */
    public String toString(){
        String str = super.toString() + "\n \t ==>>" + this.monument + " en est un monument";
        return str;
    }
}
