public class Sdz {
    public static void main(String[] args) {

        Ville v= new Ville();
        Ville v1 = new Ville("Marseille",123456789, "France" );
        Ville v2 = new Ville("Rio", 32455, "Brésil");

        System.out.println("");
        System.out.println("V= " + v.getNomVille() + " ville de " + v.getNbHabitants() + " habitants se situant en " + v.getNomPays());
        System.out.println("V1= " + v1.getNomVille() + " ville de " + v1.getNbHabitants() + " habitants se situant en " + v1.getNomPays());
        System.out.println("V2= " + v2.getNomVille() + " ville de " + v2.getNbHabitants() + " habitants se situant en " + v2.getNomPays());
        System.out.println("");

        /*
        On interchange les villes v1 & v2 par l'intermédiaire d'un autre objet Ville.
         */
        Ville temp = new Ville();
        temp = v1;
        v1 = v2;
        v2 = temp;

        System.out.println("");
        System.out.println("V1= " + v1.getNomVille() + " ville de " + v1.getNbHabitants() + " habitants se situant en " + v1.getNomPays());
        System.out.println("V2= " + v2.getNomVille() + " ville de " + v2.getNbHabitants() + " habitants se situant en " + v2.getNomPays());
        System.out.println("");

        /*
        On interchange les noms des villes par le biais des mutateurs.
         */
        v1.setNomVille("Hong Kong");;
        v2.setNomVille("Djibouti");

        System.out.println("");
        System.out.println("V1= " + v1.getNomVille() + " ville de " + v1.getNbHabitants() + " habitants se situant en " + v1.getNomPays());
        System.out.println("V2= " + v2.getNomVille() + " ville de " + v2.getNbHabitants() + " habitants se situant en " + v2.getNomPays());
        System.out.println("");

        Ville ville = new Ville("Lyon", 654, "France");
        Ville ville2 = new Ville("Lille", 123, "France");
        ville.comparer(ville2);

        System.out.println("\n\n"+v1.decrisToi());
        System.out.println(v.decrisToi());
        System.out.println(v2.decrisToi()+"\n\n");
        System.out.println(v1.comparer(v2));


        Ville a = new Ville();
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances);
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombreInstancesBis());

        Ville b = new Ville("Marseille", 1236, "France");
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances);
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombreInstancesBis());

        Ville c = new Ville("Rio", 321654, "Brésil");
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances);
        System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombreInstancesBis());
    }
}
