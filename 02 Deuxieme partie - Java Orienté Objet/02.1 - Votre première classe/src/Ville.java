

public class Ville {
    //Attributs :
    private String nomVille, nomPays;
    private int nbHabitants;
    private char categorie;
    //Variables publiques qui comptent les instances
    public static int nbreInstances = 0;
    //Variable privée qui comptera aussi les instances
    private static int nbreInstancesBis = 0;


    //Constructeur par défaut :
    public Ville() {
        System.out.println("Création d'une ville !");
        setNomVille("inconnu");
        setNomPays("inconnu");
        setNbHabitants(0);
        this.setCategorie();
        //On incrémente nos variables à chaque appel aux constructeurs
        nbreInstances++;
        nbreInstancesBis++;
        //Le reste ne change pas
    }

    //Constructeur avec paramètres :
    public Ville(String pNom, int pNbre, String pPays ) {
        System.out.println("Création d'une ville avec des paramètres.");
        setNomVille(pNom);
        setNomPays(pPays);
        setNbHabitants(pNbre);
        this.setCategorie();
        //On incrémente nos variables à chaque appel aux constructeurs
        nbreInstances++;
        nbreInstancesBis++;
        //Le reste ne change pas

    }

    //Accesseurs et mutateurs :
    //retourne le nom de la ville
    public String getNomVille() { return nomVille; }

    //définit le nom de la ville
    public void setNomVille(String nomVille) { this.nomVille = nomVille; }

    //retourne le nom du pays
    public String getNomPays() { return nomPays; }

    //Définit le nom du pays
    public void setNomPays(String nomPays) { this.nomPays = nomPays; }

    //Retourne le nombre d'habitants
    public int getNbHabitants() { return nbHabitants; }

    //Définit le nombre d'habitants
    public void setNbHabitants(int nbHabitants) {
        this.nbHabitants = nbHabitants;
        this.setCategorie();
    }

    //Retourne la catégorie de la ville
    public char getCategorie() { return categorie; }

    //Définit la catégorie de la ville
    private void setCategorie() {
        int bornesSuperieurs[] = {0, 1000, 10000, 100000, 500000, 1000000, 5000000, 10000000};
        char categories[] = {'?', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',};

        int i = 0;
        while (i < bornesSuperieurs.length && this.nbHabitants > bornesSuperieurs[i])
            i++;

        this.categorie = categories[i];
    }

    //Autres méthodes :

    /**
     * Méthode qui permet de décrire une ville.
     * @return - la description de la ville
     */
    public String decrisToi() {
        return "\t" + this.nomVille + " est une ville de "
                    + this.nomPays + ", elle comporte : "
                    + this.nbHabitants + " habitant(s) => elle est donc de catégorie : "
                    + this.categorie;
    }

    /**
     * Permet de comparer le nombre d'habitants entre 2 villes.
     * @param v1 - Une ville;
     * @return - Une chaine de caractères selon le résultat de la comparaison.
     */
    public String comparer(Ville v1) {
        String str = new String();
        if(v1.getNbHabitants() > this.nbHabitants) {
            str = v1.getNomVille() + " est un ville plus peuplée que " + this.nomVille;
        } else {
            str = this.nomVille + " est plus peuplée que " + v1.getNomVille();
        }
        return str;
    }

    /**
     * Permet de connaitre le nombre de fois où l'on a fait appel aux constructeurs de ville.
     * @return - Le nombre d'instances de villes.
     */
    public static int getNombreInstancesBis() {
        return nbreInstancesBis;
    }
}


